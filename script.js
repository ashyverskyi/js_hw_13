/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
   Метод event.preventDefault() в JavaScript используется для отмены действия по умолчанию браузера в ответ на событие. Браузер перестает выполнять стандартное действие, связанное с этим событием.

2. В чому сенс прийому делегування подій?
   Приём делегирования событий в JavaScript заключается в привязке обработчика событий к родительскому элементу, который находится выше в иерархии DOM, вместо привязки обработчика к каждому дочернему элементу отдельно.

3. Які ви знаєте основні події документу та вікна браузера?
   DOMContentLoaded, load, unload, beforeunload


Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

document.addEventListener('DOMContentLoaded', function () {
    const tabs = document.querySelectorAll('.tabs-title');
    const tabContents = document.querySelectorAll('.tabs-content li');

    function switchTab(tabIndex) {
        tabContents.forEach(content => {
            content.style.display = 'none';
        });
        tabs.forEach(tab => {
            tab.classList.remove('active');
        });
        tabContents[tabIndex].style.display = 'block';
        tabs[tabIndex].classList.add('active');
    }

    switchTab(0);

    function tabClickHandler(event) {
        if (event.target.classList.contains('tabs-title')) {
            const tabIndex = [...tabs].indexOf(event.target);
            switchTab(tabIndex);
        }
    }

    function tabKeyPressHandler(event) {
        if (event.key === 'Tab') {
            const activeTabIndex = [...tabs].findIndex(tab => tab.classList.contains('active'));
            const nextTabIndex = (activeTabIndex + 1) % tabs.length;
            switchTab(nextTabIndex);
            event.preventDefault();
        }
    }

    document.addEventListener('keydown', tabKeyPressHandler);
    document.querySelector('.tabs').addEventListener('click', tabClickHandler);
});



